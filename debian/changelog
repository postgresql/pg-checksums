pg-checksums (1.2-3) UNRELEASED; urgency=medium


 -- Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>  Fri, 04 Oct 2024 14:19:49 +0200

pg-checksums (1.2-2) unstable; urgency=medium

  * Source-only upload.

 -- Michael Banck <mbanck@debian.org>  Fri, 04 Oct 2024 14:19:34 +0200

pg-checksums (1.2-1) unstable; urgency=medium

  * New upstream release.
  * Upload for PostgreSQL 17.
  * debian/patches/fix_regression_tests_v16.patch: Removed, no longer needed.
  * debian/patches/fix_page_is_new_build_failure.patch: Likewise.
  * debian/pgversion: Bump to 11+.

 -- Michael Banck <mbanck@debian.org>  Fri, 20 Sep 2024 21:09:30 +0200

pg-checksums (1.1-7) unstable; urgency=medium

  * debian/patches/fix_page_is_new_build_failure.patch: New patch, fixes
    build failures with PostgreSQL 16 on newer GCC releases (Closes: #1075381).

 -- Michael Banck <mbanck@debian.org>  Tue, 13 Aug 2024 16:23:56 +0200

pg-checksums (1.1-6) unstable; urgency=medium

  * Source-only team upload.

 -- Christoph Berg <myon@debian.org>  Sat, 23 Sep 2023 10:28:10 +0200

pg-checksums (1.1-5) unstable; urgency=medium

  [ Michael Banck ]
  * debian/patches/fix_regression_tests_v16.patch: New patch, fixes regression
    tests with PostgreSQL v16, taken from upstream commit 2b1fddb.

  [ Christoph Berg ]
  * Upload for PostgreSQL 16.
  * Use ${postgresql:Depends}.

 -- Christoph Berg <myon@debian.org>  Wed, 20 Sep 2023 16:33:55 +0000

pg-checksums (1.1-4) unstable; urgency=medium

  * debian/control.in, debian/control (Uploaders): Updated.

 -- Michael Banck <mbanck@debian.org>  Fri, 17 Feb 2023 14:08:41 +0100

pg-checksums (1.1-3) unstable; urgency=medium

  * Source-only team upload.

 -- Christoph Berg <myon@debian.org>  Wed, 02 Nov 2022 12:14:47 +0100

pg-checksums (1.1-2) unstable; urgency=medium

  * Team upload for PostgreSQL 15.
  * debian/watch: Look at GitHub tags instead of releases.

 -- Christoph Berg <myon@debian.org>  Fri, 21 Oct 2022 10:34:30 +0200

pg-checksums (1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/pg-checksums-doc.manpages: Updated for new program binary name. 
  * debian/control, debian/control.in: Likewise.
  * debian/postgresql-{12,13,14}-pg-checksums.{preinst,postrm}: Removed.
  * debian/postgresql-{12,13,14}-pg-checksums.{prerm,postinst}: New files,
    remove the diversion.

 -- Michael Banck <michael.banck@credativ.de>  Mon, 17 Jan 2022 21:29:44 +0100

pg-checksums (1.0-8) unstable; urgency=medium

  * Source-only team upload.

 -- Christoph Berg <myon@debian.org>  Tue, 02 Nov 2021 11:24:38 +0100

pg-checksums (1.0-7) unstable; urgency=medium

  * Team upload for PostgreSQL 14.

 -- Christoph Berg <myon@debian.org>  Fri, 15 Oct 2021 14:19:34 +0200

pg-checksums (1.0-6) unstable; urgency=medium

  * Team upload.
  * Rules-Requires-Root: no.

 -- Christoph Berg <myon@debian.org>  Sat, 17 Oct 2020 11:48:22 +0200

pg-checksums (1.0-5) unstable; urgency=medium

  * Team upload.
  * Use DIR instead of PATH in maintainer scripts.

 -- Christoph Berg <myon@debian.org>  Mon, 05 Oct 2020 16:46:48 +0200

pg-checksums (1.0-4) unstable; urgency=low

  * Team upload for PostgreSQL 13.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 10.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Christoph Berg ]
  * Add maintainer scripts for PostgreSQL 13 and 14.
  * Use dh --with pgxs.
  * DH 13.
  * debian/tests: Use 'make' instead of postgresql-server-dev-all.
  * debian/tests: Use installed-versions instead of supported-versions.

 -- Christoph Berg <myon@debian.org>  Mon, 05 Oct 2020 16:13:56 +0200

pg-checksums (1.0-3) unstable; urgency=medium

  * Source-only team upload.

 -- Christoph Berg <myon@debian.org>  Mon, 11 Nov 2019 20:33:15 +0100

pg-checksums (1.0-2) unstable; urgency=medium

  * Team upload for PostgreSQL 12.
  * pg-checksums-doc: Fix section to be "doc".
  * Mark pg-checksums-doc as Multi-Arch: foreign.

 -- Christoph Berg <myon@debian.org>  Tue, 29 Oct 2019 13:01:43 +0100

pg-checksums (1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Banck <michael.banck@credativ.de>  Tue, 22 Oct 2019 21:18:51 +0200

pg-checksums (0.13-2) unstable; urgency=medium

  * debian/control: Bump pg-checksums-doc from Suggests to Recommends.
  * debian/control: Fix pg-checksums-doc Section-header.
  * debian/postgresql-12-pg-checksums.preinst,
    debian/postgresql-12-pg-checksums.postrm: Add maintainer scripts doing
    dpkg-divert handling for upstream pg_checksums to pg_checksums.pgdg.
  * debian/pgversions: Re-add postgresql-12-pg-checksums package by no longer
    restricting the list of supported PostgreSQL versions.

 -- Michael Banck <michael.banck@credativ.de>  Tue, 01 Oct 2019 09:17:46 +0200

pg-checksums (0.13-1) unstable; urgency=medium

  * New upstream release.

  [ Christoph Berg ]
  * Add docbook-xsl build-dependency.

  [ Michael Banck ]
  * debian/patches/jessie_build_failure.patch: Removed, no longer needed.
  * debian/patches/postgresnode.patch: Likewise.

 -- Michael Banck <michael.banck@credativ.de>  Fri, 27 Sep 2019 15:37:21 +0200

pg-checksums (0.12-3) unstable; urgency=medium

  * debian/rules (override_dh_auto_build): Make man target.
  * debian/control (Build-Depends): Added xsltproc.
  * debian/control (pg-checksums-doc): New package.
  * debian/pg-checksums-doc.manpages: Install manpage into pg-checksums-doc
    package.

 -- Michael Banck <michael.banck@credativ.de>  Wed, 14 Aug 2019 09:19:05 +0200

pg-checksums (0.12-2) unstable; urgency=medium

  * debian/patches/jessie_build_failure.patch: New patch, avoids a for-loop
    initial declaration, allowing to build on jessie.

 -- Michael Banck <michael.banck@credativ.de>  Sat, 10 Aug 2019 18:38:57 +0200

pg-checksums (0.12-1) unstable; urgency=medium

  * New upstream releae.
  * debian/patches/jessie_build_failure.patch: Removed, no longer needed.

 -- Michael Banck <michael.banck@credativ.de>  Sat, 10 Aug 2019 18:21:37 +0200

pg-checksums (0.11-2) unstable; urgency=medium

  * debian/patches/jessie_build_failure.patch: New patch, avoids a for-loop
    initial declaration, allowing to build on jessie, taken from upstream
    commit 931ed97.

 -- Michael Banck <michael.banck@credativ.de>  Mon, 05 Aug 2019 16:13:33 +0200

pg-checksums (0.11-1) unstable; urgency=medium

  * New upstream release.

  [ Michael Banck ]
  * debian/patches/fsync_pgdata.patch: Removed, no longer needed.
  * debian/patches/zero_random_pageheader.patch: Likewise.
  * debian/pgversions: Do not support v12 for now.

  [ Christoph Berg ]
  * Update debian/gitlab-ci.yml

 -- Michael Banck <michael.banck@credativ.de>  Mon, 05 Aug 2019 11:20:36 +0200

pg-checksums (0.8-3) unstable; urgency=medium

  [ Christoph Berg ]
  * Remove myself from uploaders.

  [ Michael Banck ]
  * debian/patches/zero_random_pageheader.patch: New patch, fixes false
    negatives for all-zero or random pageheaders which were not detected as
    checksum failures previously. Adapted from upstream commit 043f003f
    (Closes: #928232).

 -- Michael Banck <michael.banck@credativ.de>  Tue, 30 Apr 2019 14:55:21 +0200

pg-checksums (0.8-2) unstable; urgency=medium

  * debian/patches/fsync_pgdata.patch: New patch, backports the fsync_pgdata()
    method from PostgreSQL 10 instead of relying on `initdb -S' for syncing,
    which is fragile.

 -- Michael Banck <michael.banck@credativ.de>  Sat, 09 Mar 2019 16:14:40 +0100

pg-checksums (0.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/fix_tablespace_scanning.patch: Removed, no longer needed.

 -- Michael Banck <michael.banck@credativ.de>  Fri, 30 Nov 2018 13:14:35 +0100

pg-checksums (0.7-2) unstable; urgency=medium

  * debian/patches/fix_tablespace_scanning.patch: New patch, fixes scanning of
    tablespaces, taken from upstream commit a444a315.

 -- Michael Banck <michael.banck@credativ.de>  Tue, 27 Nov 2018 16:48:22 +0100

pg-checksums (0.7-1) unstable; urgency=medium

  * New upstream version.
  * debian/tests/prove: Dump all logfiles on failure.
  * debian/patches/no-streaming: Removed, no longer needed.

 -- Michael Banck <mbanck@debian.org>  Mon, 26 Nov 2018 13:00:50 +0100

pg-checksums (0.6-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <christoph.berg@credativ.de>  Mon, 19 Nov 2018 10:52:28 +0100

pg-checksums (0.5-3) unstable; urgency=medium

  * debian/gitlab-ci.yml: New file.
  * debian/patches/psprintf_fix.patch: New patch, reverts changes introducing
    psprintf().

 -- Michael Banck <michael.banck@credativ.de>  Tue, 16 Oct 2018 16:11:05 +0200

pg-checksums (0.5-2) unstable; urgency=medium

  * Upload for PostgreSQL 11.

 -- Christoph Berg <myon@debian.org>  Thu, 11 Oct 2018 22:46:40 +0200

pg-checksums (0.5-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <christoph.berg@credativ.de>  Wed, 10 Oct 2018 15:05:34 +0200

pg-checksums (0.4-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <christoph.berg@credativ.de>  Fri, 31 Aug 2018 10:30:51 +0200

pg-checksums (0.3-1) unstable; urgency=medium

  * New upstream version with support for online checksum verification.
  * Move maintainer address to team+postgresql@tracker.debian.org.

 -- Christoph Berg <myon@debian.org>  Sat, 14 Jul 2018 11:52:14 +0200

pg-checksums (0.2-1) unstable; urgency=medium

  * New upstream version with 9.3 support.
  * debian/patches/postgresnode.patch: Include SimpleTee.pm (missing on 9.3).
  * Remove test-dependency on pgtap, not needed.
  * Bump S-V.
  * Add myself to uploaders.

 -- Christoph Berg <myon@debian.org>  Mon, 21 May 2018 10:28:17 +0200

pg-checksums (0.1-1) unstable; urgency=medium

  * Team upload.
  * Initial release.

 -- Michael Banck <michael.banck@credativ.de>  Thu, 18 May 2017 20:51:02 +0200
